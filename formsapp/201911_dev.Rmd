---
title: Shiny Forms Development
author: "Pieta Schofield"
output:
  bookdown::html_document2:
    code_folding: hide
    keep_md: yes
    number_sections: no
    fig_caption: yes
    fig_width: 8
    fig_height: 8
    highlight: textmate
    includes:
        before_body: ../include/before_body_live.html
    toc: yes
    toc_depth: 4
    css: ../include/live.css
bibliography: ../../motley/biblio/zoterofull.bib
---

```{r,echo=FALSE, include=F}
require(plib)
require(knitr)
require(RDocumentation)
require(kableExtra)
.projName <- "formsapp"
.codeDir <- file.path(Sys.getenv("HOME"),"GitLab")
.gitRepo <- "shinypph"
.fileName <- "201911_dev"
if(FALSE){
}
opts_chunk$set( message=F, warning=F,comment=NA)
#
# Alter this section to setup the paths for ease later on
#
.homeDir <- file.path(Sys.getenv("HOME"))
.projDir <- file.path(.homeDir,"Projects",.projName)
.dataDir <- file.path(.projDir,"Data")
dir.create(.dataDir,recur=T,showW=F)
```


# Build Information 

```{r compile-info, echo=F}
cat(paste0(file.path(.codeDir,.gitRepo,.projName,paste0(.fileName,".Rmd"))," compiled ",Sys.time(),
           " on ",Sys.info()["nodename"]," by ",Sys.info()["user"]))
```

# References

```{r eval=F}
plib::rc(fileName="201911_dev",projName="formsapp",gitRepo="shinypph", livUP=T,pphUP=F)
```
